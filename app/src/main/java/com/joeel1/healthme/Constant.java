package com.joeel1.healthme;

/**
 * Created by ariffin.ahmad on 16/01/2018.
 */

public class Constant {

    public static String TAG               = "HealthMe";
    public static String WEB_CLIENT_ID     = "433456614138-un3blm7rjh8l29uknk3b7ba8dhkti494.apps.googleusercontent.com";

    public static int SIGNIN_INTERVAL_SECS = 5;

    public static String URL_USER_API      = "http://healthme.azurewebsites.net";
    public static String USER_API_DISTANCE = "/user/distance";
}
