package com.joeel1.healthme;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by ariffin.ahmad on 19/07/2017.
 */

public class CardHospitalItem extends RelativeLayout {
    private TextView txt_name;
    private TextView txt_distance;

    public CardHospitalItem(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.card_hospital, this, true);

        txt_name        = findViewById(R.id.card_hospital_name);
        txt_distance    = findViewById(R.id.card_hospital_distance);
    }

    public void update(DataStructureHospital hospital) {
        txt_name.setText(hospital.name);
        if (hospital.distance < 1000)
            txt_distance.setText(hospital.distance + " m");
        else
            txt_distance.setText(String.format("%.1f", (float)hospital.distance / 1000f) + " km");
    }
}