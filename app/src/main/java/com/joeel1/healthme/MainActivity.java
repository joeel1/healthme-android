package com.joeel1.healthme;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.joeel1.healthme.auth.GoogleAuthSilent;
import com.joeel1.healthme.utils.AbsWorkerThread;
import com.joeel1.healthme.utils.JSONRequester;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final ArrayList<DataStructureHospital> hospitals = new ArrayList<>();
    private JSONRequester json_requester                     = new JSONRequester();

    private int count_signing                                = 0;
    private GoogleAuthSilent silent_singin;
    private boolean flag_authenticating                      = false;
    private SigningWorker signing_worker                     = new SigningWorker();

    private Handler handler;
    private HospitalAdapter adapter                          = new HospitalAdapter();
    private RecyclerView.LayoutManager layout_manager;
    private TextView text_status;
    private RecyclerView recycler_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handler                 = new Handler();
        setContentView(R.layout.activity_main);

        recycler_view           = findViewById(R.id.main_recyclerview);
        text_status             = findViewById(R.id.main_status);

        layout_manager          = new LinearLayoutManager(this);
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(layout_manager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setAdapter(adapter);

        silent_singin           = new GoogleAuthSilent(this, Constant.WEB_CLIENT_ID);
        silent_singin.callback  = new GoogleAuthSilent.IGoogleAuthSilent() {
            @Override
            public void onSignInResult(GoogleSignInResult result) {
                flag_authenticating = false;
                if (result != null && result.isSuccess()) {
                    count_signing   = 0;
                    Log.wtf(Constant.TAG, ">>> site activity: Success signing on attempt " + count_signing);
                    GoogleSignInAccount account = result.getSignInAccount();
                    String url_params = "?lat=3.055549&lon=101.701008&token=" + account.getIdToken();
                    json_requester.start(Constant.URL_USER_API + Constant.USER_API_DISTANCE  + url_params, 0);
                }
                else if (count_signing <= 5)
                    signing_worker.start(Constant.SIGNIN_INTERVAL_SECS);
            }
        };

        json_requester.callback = new DistanceListener();
    }

    @Override
    public void onResume() {
        super.onResume();
        signing_worker.start();

    }

    @Override
    public void onPause() {
        super.onPause();
    }


    private class HospitalViewHolder extends RecyclerView.ViewHolder {
        public final CardHospitalItem hospital_item;

        public HospitalViewHolder(CardHospitalItem hospital_item) {
            super(hospital_item);

            this.hospital_item = hospital_item;
            hospital_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DataStructureHospital hospital = hospitals.get(getAdapterPosition());
                }
            });
        }
    }

    private class HospitalAdapter extends RecyclerView.Adapter<HospitalViewHolder> {

        @Override
        public HospitalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new HospitalViewHolder(new CardHospitalItem(MainActivity.this, null));
        }

        @Override
        public void onBindViewHolder(HospitalViewHolder holder, int position) {
            if (position >= hospitals.size())
                return;

            DataStructureHospital hospital = hospitals.get(position);
            holder.hospital_item.update(hospital);
        }

        @Override
        public int getItemCount() {
            int count = hospitals.size();

            return count;
        }
    }


        private class SigningWorker extends AbsWorkerThread {
        private int wait_secs = 0;

        public void start(int wait_secs) {
            this.wait_secs = wait_secs;
            start();
        }

        @Override
        public void process() {
            count_signing       += 1;
            flag_authenticating  = true;
            Log.wtf(Constant.TAG, ">>> site activity: Signing - attempt " + count_signing + " of " + 5);
            if (wait_secs > 0) {
                try { Thread.sleep(wait_secs * 1000); } catch (InterruptedException e) { return; }
            }
            if (!stop.get())
                silent_singin.silentSignIn();
        }
    }


    private class DistanceListener implements JSONRequester.IJSONRequester {

        @Override
        public void onJSONRequestStart() {
            Log.wtf(Constant.TAG, "Main>>> distance request start");
        }

        @Override
        public void onJSONRequestDone(JSONArray json_array) {
            Log.wtf(Constant.TAG, "Main>>> distance request done: array count " + json_array.length());
            hospitals.clear();
            for (int i = 0; i < json_array.length(); i++) {
                try {
                    JSONObject json   = json_array.getJSONObject(i);
                    int id            = json.getInt("id");
                    String name       = json.getString("name");
                    String lat        = json.getString("lat");
                    String lon        = json.getString("lon");
                    String address    = json.getString("address");
                    int distance      = json.getInt("distance");
                    int duration_secs = json.getInt("duration_secs");;

                    hospitals.add(new DataStructureHospital(id, name, lat, lon, address, distance, duration_secs));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            handler.post(new Runnable() {
                @Override
                public void run() {
                    text_status.setVisibility(View.INVISIBLE);
                    adapter.notifyDataSetChanged();
                }
            });
        }

        @Override
        public void onJSONRequestDone(JSONObject json) {
            Log.wtf(Constant.TAG, "Main>>> distance request done: json " + json.toString());
        }

        @Override
        public void onJSONRequestError(Exception e) {
            Log.wtf(Constant.TAG, "Main>>> distance request error - " + e.getMessage());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    text_status.setVisibility(View.VISIBLE);
                    text_status.setText("error while querying distance...");
                }
            });
        }
    }
}
