package com.joeel1.healthme;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.joeel1.healthme.auth.GoogleAuth;

import java.util.ArrayList;

public class AuthActivity extends AppCompatActivity
{
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL = 9999;
    private static final int RC_SIGN_IN                           = 9001;

    private GoogleAuth google_auth;
    private SharedPreferences preferences;

    private HealthMeApplication app;
    private RelativeLayout main_layout;
    private SignInButton btn_si;
    private LinearLayout layout_progress;
    private TextView txt_status;
    private Handler handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        handler              = new Handler();
        app                  = (HealthMeApplication) getApplication();

        preferences          = PreferenceManager.getDefaultSharedPreferences(this);
        google_auth          = new GoogleAuth(this, Constant.WEB_CLIENT_ID);
        google_auth.callback = new GoogleAuth.IGoogleAuth() {
            @Override
            public void onAuthConnected() {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(google_auth.google_api_client);
                startActivityForResult(intent, RC_SIGN_IN);
            }
        };

        main_layout          = (RelativeLayout) findViewById(R.id.auth_main_layout);
        btn_si               = (SignInButton) findViewById(R.id.auth_btn_signin);
        layout_progress      = (LinearLayout) findViewById(R.id.auth_progress);
        txt_status           = (TextView) findViewById(R.id.auth_status);
        TextView txt_version = (TextView) findViewById(R.id.auth_version);

        //map_status_requester.callback = new JSONRequesterListener();
        try {
            PackageInfo info = app.getPackageManager().getPackageInfo(app.getPackageName(), 0);
            String version   = info.versionName;
            txt_version.setText("version: " + version);
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Log.wtf(Constant.TAG, "on create....");
        gotPermissions();
    }

    private void gotPermissions() {
        Log.wtf(Constant.TAG, "check permission - has signed in: " + GoogleAuth.hasSignedIn(preferences) + "....");

        ArrayList<String> tmp_permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED)
            tmp_permissions.add(Manifest.permission.INTERNET);
//        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
//            tmp_permissions.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
//        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
//            tmp_permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
//            tmp_permissions.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
//        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_WIFI_STATE) != PackageManager.PERMISSION_GRANTED)
//            tmp_permissions.add(android.Manifest.permission.ACCESS_WIFI_STATE);
//        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CHANGE_WIFI_STATE) != PackageManager.PERMISSION_GRANTED)
//            tmp_permissions.add(android.Manifest.permission.CHANGE_WIFI_STATE);

        if (tmp_permissions.size() > 0)
            ActivityCompat.requestPermissions(this, tmp_permissions.toArray(new String[tmp_permissions.size()]), MY_PERMISSIONS_REQUEST_READ_EXTERNAL);
        else {
            if (GoogleAuth.hasSignedIn(preferences)) {
                Intent intent = new Intent(AuthActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
            else {
                btn_si.setColorScheme(SignInButton.COLOR_LIGHT);
                btn_si.setSize(SignInButton.SIZE_WIDE);
                btn_si.setScopes(google_auth.getScopeArray());
                btn_si.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.wtf(Constant.TAG, "button signing in clicked.....");
                        google_auth.connect();
                    }
                });
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    @Override
    public void onActivityResult(int request_code, int result_code, Intent data) {
        super.onActivityResult(request_code, result_code, data);

        if (request_code == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.wtf(Constant.TAG, "got permission result");
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL:
                gotPermissions();
                return;
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(Constant.TAG, "handleSignInResult: " + result.isSuccess());
        Log.d(Constant.TAG, "handleSignInResult: account " + result.getSignInAccount());
        Log.d(Constant.TAG, "handleSignInResult: status " + result.getStatus());
        if (result.isSuccess()) {
            GoogleSignInAccount acc = result.getSignInAccount();
            String str_signin       = "signed in....\n";
            str_signin             += "=============\n";
            str_signin             += "name: '" + acc.getDisplayName() + "'\n";
            str_signin             += "e-mail: '" + acc.getEmail() + "'\n";
            str_signin             += "id: '" + acc.getId() + "'\n";
            str_signin             += "photo url: '" + acc.getPhotoUrl() + "'\n";
            str_signin             += "id token: '" + acc.getIdToken() + "'\n";
            Log.i(Constant.TAG, str_signin);

            app.signed_in           = GoogleAuth.signingIn(preferences, acc.getDisplayName(), acc.getGivenName(), acc.getFamilyName(), acc.getEmail(), acc.getId(), acc.getPhotoUrl().toString());
            google_auth.disconnect();

            Intent intent = new Intent(AuthActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Log.i(Constant.TAG, "failed signing in....");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        //map_status_requester.stop();
    }
}