package com.joeel1.healthme;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.joeel1.healthme.auth.AbsGoogleAuth;
import com.joeel1.healthme.auth.SignedInDataStructure;

/**
 * Created by ariffin.ahmad on 16/01/2018.
 */

public class HealthMeApplication extends Application {

    public SignedInDataStructure signed_in = null;

    private SharedPreferences preferences        = null;
    private SharedPreferences.Editor pref_editor = null;

    @Override
    public void onCreate() {
        super.onCreate();

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        pref_editor = preferences.edit();
        signed_in   = AbsGoogleAuth.loadSignIn(preferences);
    }
}
