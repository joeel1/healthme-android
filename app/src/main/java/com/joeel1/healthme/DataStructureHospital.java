package com.joeel1.healthme;

/**
 * Created by ariffin.ahmad on 16/01/2018.
 */

public class DataStructureHospital {
    public final int id;
    public final String name;
    public final String lat;
    public final String lon;
    public final String address;
    public final int distance;
    public final int duration_secs;

    public DataStructureHospital(int id, String name, String lat, String lon, String address, int distance, int duration_secs) {
        this.id            = id;
        this.name          = name;
        this.lat           = lat;
        this.lon           = lon;
        this.address       = address;
        this.distance      = distance;
        this.duration_secs = duration_secs;
    }
}
